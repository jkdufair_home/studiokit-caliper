﻿using StudioKit.Caliper.Exceptions;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Caliper.Controllers.API
{
	[RoutePrefix("api/caliper-eventstore")]
	public class CaliperEventStoreController : ApiController
	{
		private readonly CaliperEventStoreService _caliperEventStoreService;

		public CaliperEventStoreController(CaliperEventStoreService caliperEventStoreService)
		{
			_caliperEventStoreService = caliperEventStoreService;
		}

		[HttpGet]
		[Route("token")]
		public async Task<IHttpActionResult> GetTokenAsync(CancellationToken cancellationToken)
		{
			try
			{
				var token = await _caliperEventStoreService.GetTokenAsync(cancellationToken);
				return Ok(token);
			}
			catch (CaliperNotEnabledException)
			{
				return ResponseMessage(new HttpResponseMessage(HttpStatusCode.Forbidden));
			}
		}
	}
}