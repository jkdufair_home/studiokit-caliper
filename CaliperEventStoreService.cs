﻿using Newtonsoft.Json;
using StudioKit.Caliper.Exceptions;
using StudioKit.Caliper.Interfaces;
using StudioKit.Encryption;
using StudioKit.TransientFaultHandling.Http;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Caliper
{
	public class CaliperEventStoreService
	{
		private readonly ICaliperConfigurationProvider _configurationProvider;

		public CaliperEventStoreService(ICaliperConfigurationProvider configurationProvider)
		{
			_configurationProvider = configurationProvider;
		}

		public async Task<Dictionary<string, string>> GetTokenAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var configuration = await _configurationProvider.GetCaliperConfigurationAsync(cancellationToken);
			if (configuration == null)
				throw new Exception("Could not load ICaliperConfiguration from ICaliperConfigurationProvider");

			if (!configuration.CaliperEnabled)
				throw new CaliperNotEnabledException();

			var hostname = EncryptedConfigurationManager.TryDecryptSettingValue(configuration.CaliperEventStoreHostname);
			if (string.IsNullOrWhiteSpace(hostname))
				throw new CaliperConfigurationException("ICaliperConfiguration.CaliperEventStoreHostname is required");

			var clientId = EncryptedConfigurationManager.TryDecryptSettingValue(configuration.CaliperEventStoreClientId);
			if (string.IsNullOrWhiteSpace(clientId))
				throw new CaliperConfigurationException("ICaliperConfiguration.CaliperEventStoreClientId is required");

			var clientSecret = EncryptedConfigurationManager.TryDecryptSettingValue(configuration.CaliperEventStoreClientSecret);
			if (string.IsNullOrWhiteSpace(clientId))
				throw new CaliperConfigurationException("ICaliperConfiguration.CaliperEventStoreClientSecret is required");

			var tokenEndpointUri = new Uri($"https://{hostname}{CaliperConstants.OAuthTokenPath}");

			// create client
			var client = new RetryingHttpClient();

			// POST to caliper eventstore
			string responseBody;
			try
			{
				responseBody = await client
					.PostAsync(tokenEndpointUri, uri => new FormUrlEncodedContent(new[]
					{
						new KeyValuePair<string, string>("client_id", clientId),
						new KeyValuePair<string, string>("client_secret", clientSecret),
						new KeyValuePair<string, string>("grant_type", "client_credentials"),
					}), cancellationToken)
					.ConfigureAwait(false);
			}
			catch (HttpRequestException ex)
			{
				throw new CaliperEventStoreRequestException("Caliper EventStore API error", ex);
			}

			// parse json
			var token = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseBody);

			return token;
		}
	}
}