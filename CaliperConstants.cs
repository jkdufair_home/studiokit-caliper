﻿namespace StudioKit.Caliper
{
	public static class CaliperConstants
	{
		public static string OAuthTokenPath = "/oauth/token";

		public static string PurduePersonNamespace = "https://purdue.edu/user/";
	}
}