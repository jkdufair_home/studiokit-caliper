﻿using System;

namespace StudioKit.Caliper.Exceptions
{
	public class CaliperEventStoreRequestException : Exception
	{
		public CaliperEventStoreRequestException()
		{
		}

		public CaliperEventStoreRequestException(string message)
			: base(message) { }

		public CaliperEventStoreRequestException(string message, Exception inner)
			: base(message, inner) { }
	}
}